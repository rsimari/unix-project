CSE-20189-SP16 - Projects
=========================

This is the group repository for the final two projects in [CSE-20189-SP16].

Group Members
-------------

- Robert Simari (rsimari@nd.edu)
- John Joyce (jjoyce6@nd.edu)

[CSE-20189-SP16]: https://www3.nd.edu/~pbui/teaching/cse.20189.sp16/